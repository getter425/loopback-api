var app = angular.module('todoApp', ['lbServices']);
 
app.controller('todoController', function($scope, $http, Product) {
 
 	$scope.products= Product.find();
 	$scope.product;
 	$scope.loading=false;
 
  	$scope.add = function(){
  		$scope.loading=true;
  		
  		Product.create({title: $scope.product.title,price:0 }).$promise
 			 .then(function(product) { 
 			 		$scope.products.push(product);
 			 		$scope.product.title='';
 			 		$scope.loading=false;
 			  });;
  	};
 
  	$scope.delete = function($index){
  		
  		$scope.loading=true;
  		var product = $scope.products[$index];
  		
  		Product.deleteById({ id: product.id}).$promise
  		    .then(function() {
				$scope.products.splice($index,1);
				$scope.loading=false;
		     });
  	};
 
  	$scope.update = function(product){
  		product.$save();
  	};
	
});
